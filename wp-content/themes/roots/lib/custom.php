<?php
/**
 * Custom functions
 *
 * More for misc. functions, not a catch all
 *
 * Use:
 * image-sizes.php - to manage image sizes
 * widgets.php     - to manage widget areas
 */

add_action( 'phpmailer_init', 'bish_configMH', 10, 1 );
function bish_configMH( $phpmailer ) {
	// Define that we are sending with SMTP
	$phpmailer->isSMTP();
	// The hostname of the mailserver
	$phpmailer->Host = 'localhost';
	// Use SMTP authentication (true|false)
	$phpmailer->SMTPAuth = false;
	// SMTP port number
	// Mailhog normally run on port 1025
	$phpmailer->Port = WP_DEBUG ? '1025' : '25';
	// Username to use for SMTP authentication
	// $phpmailer->Username = 'yourusername';
	// Password to use for SMTP authentication
	// $phpmailer->Password = 'yourpassword';
	// The encryption system to use - ssl (deprecated) or tls
	// $phpmailer->SMTPSecure = 'tls';
	$phpmailer->From = 'site_adm@wp.local';
	$phpmailer->FromName = 'WP DEV';
}


add_action('wp_ajax_nopriv_fahadsending_mail', 'fahadsending_mail');
add_action('wp_ajax_fahadsending_mail', 'fahadsending_mail');

function fahadsending_mail(){

    $to = "michael@lemonadestand.org";
    $subject = "Donation";
    $message = "message message message message message message message "; 

    if( wp_mail($to, $subject, $message) ){
        echo "mail sent";
    } else {
        echo "mail not sent";
    }

    die(); // never forget to die() your AJAX reuqests

}


/*
add_action( 'init', 'custom_init', 10, 1 );
function custom_init(  ) {
	// Define that we are sending with SMTP
	if (!empty($_POST["email"])) {
		print_r($_POST);

		$to = 'michael@lemonadestand.org';
		$subject = 'Solar Smart Buy';
		$body = 'Brian did we win?';

		$send = wp_mail( $to, $subject, $body );

		print_r($send);

	}




}
*/

/*
// if you want only logged in users to access this function use this hook
add_action('wp_ajax_mail_before_submit', 'mycustomtheme_send_mail_before_submit');

// if you want none logged in users to access this function use this hook
add_action('wp_ajax_nopriv_mail_before_submit', 'mycustomtheme_send_mail_before_submit');

// if you want both logged in and anonymous users to get the emails, use both hooks above

function mycustomtheme_send_mail_before_submit(){
    check_ajax_referer('my_email_ajax_nonce');
    if ( isset($_POST['action']) && $_POST['action'] == "mail_before_submit" ){

    //send email  wp_mail( $to, $subject, $message, $headers, $attachments ); ex:
        wp_mail('<?php echo $email; ?>','this is the email subject line','email message body');
        echo 'email sent';
        die();
    }
    echo 'error';
    die();
}

*/